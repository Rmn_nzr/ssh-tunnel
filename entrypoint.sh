#!/bin/bash

set -e

if [[ -n ${PRIVATE_KEY} ]]
then
    echo $PRIVATE_KEY > /private_key
fi

chmod 600 /private_key

if [[ "$MODE" == "L" ]]
then
    /usr/bin/ssh -o StrictHostKeyChecking=no -i /private_key -p $SSH_PORT -N -L 0.0.0.0:$LOCAL_PORT:$TARGET_IP:$SERVER_PORT  $SERVER_USER@$SERVER_IP
elif [[ "$MODE" == "R" ]]
then
    /usr/bin/ssh -o StrictHostKeyChecking=no -i /private_key -p $SSH_PORT -N -R SERVER_IP:$SERVER_PORT:localhost:$LOCAL_PORT  $SERVER_USER@$SERVER_IP
fi
